<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use UDS\Component\Config\Loader\FileLoader;
use UDS\Component\Routing\RouteCollection;

/**
 * GlobFileLoader loads files from a glob pattern.
 *

 */
class GlobFileLoader extends \UDS\Component\Config\Loader\FileLoader
{
    public function load(mixed $resource, string $type = null): mixed
    {
        $globResource = null;
        $collection = new \UDS\Component\Routing\RouteCollection();

        foreach ($this->glob($resource, false, $globResource) as $path => $info) {
            $collection->addCollection($this->import($path));
        }

        $collection->addResource($globResource);

        return $collection;
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        return 'glob' === $type;
    }
}
