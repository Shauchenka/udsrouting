<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use UDS\Component\Config\Resource\DirectoryResource;
use UDS\Component\Routing\RouteCollection;

/**
 * AnnotationDirectoryLoader loads routing information from annotations set
 * on PHP classes and methods.
 *

 */
class AnnotationDirectoryLoader extends \UDS\Component\Routing\Loader\AnnotationFileLoader
{
    /**
     * @throws \InvalidArgumentException When the directory does not exist or its routes cannot be parsed
     */
    public function load(mixed $path, string $type = null): ?\UDS\Component\Routing\RouteCollection
    {
        if (!is_dir($dir = $this->locator->locate($path))) {
            return parent::supports($path, $type) ? parent::load($path, $type) : new \UDS\Component\Routing\RouteCollection();
        }

        $collection = new \UDS\Component\Routing\RouteCollection();
        $collection->addResource(new \UDS\Component\Config\Resource\DirectoryResource($dir, '/\.php$/'));
        $files = iterator_to_array(new \RecursiveIteratorIterator(
            new \RecursiveCallbackFilterIterator(
                new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS),
                fn(\SplFileInfo $current) => !str_starts_with($current->getBasename(), '.')
            ),
            \RecursiveIteratorIterator::LEAVES_ONLY
        ));
        usort($files, fn(\SplFileInfo $a, \SplFileInfo $b) => (string) $a > (string) $b ? 1 : -1);

        foreach ($files as $file) {
            if (!$file->isFile() || !str_ends_with((string) $file->getFilename(), '.php')) {
                continue;
            }

            if ($class = $this->findClass($file)) {
                $refl = new \ReflectionClass($class);
                if ($refl->isAbstract()) {
                    continue;
                }

                $collection->addCollection($this->loader->load($class, $type));
            }
        }

        return $collection;
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        if (!\is_string($resource)) {
            return false;
        }

        if (\in_array($type, ['annotation', 'attribute'], true)) {
            return true;
        }

        if ($type) {
            return false;
        }

        try {
            return is_dir($this->locator->locate($resource));
        } catch (\Exception) {
            return false;
        }
    }
}
