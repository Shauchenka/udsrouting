<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use UDS\Component\Config\Loader\FileLoader;
use UDS\Component\Config\Resource\DirectoryResource;
use UDS\Component\Routing\RouteCollection;

class DirectoryLoader extends \UDS\Component\Config\Loader\FileLoader
{
    public function load(mixed $file, string $type = null): mixed
    {
        $path = $this->locator->locate($file);

        $collection = new \UDS\Component\Routing\RouteCollection();
        $collection->addResource(new \UDS\Component\Config\Resource\DirectoryResource($path));

        foreach (scandir($path) as $dir) {
            if ('.' !== $dir[0]) {
                $this->setCurrentDir($path);
                $subPath = $path.'/'.$dir;
                $subType = null;

                if (is_dir($subPath)) {
                    $subPath .= '/';
                    $subType = 'directory';
                }

                $subCollection = $this->import($subPath, $subType, false, $path);
                $collection->addCollection($subCollection);
            }
        }

        return $collection;
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        // only when type is forced to directory, not to conflict with AnnotationLoader

        return 'directory' === $type;
    }
}
