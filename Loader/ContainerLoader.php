<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use Psr\Container\ContainerInterface;

/**
 * A route loader that executes a service from a PSR-11 container to load the routes.
 *

 */
class ContainerLoader extends \UDS\Component\Routing\Loader\ObjectLoader
{
    public function __construct(private readonly ContainerInterface $container, string $env = null)
    {
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        return 'service' === $type && \is_string($resource);
    }

    protected function getObject(string $id): object
    {
        return $this->container->get($id);
    }
}
