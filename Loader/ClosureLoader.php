<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use UDS\Component\Config\Loader\Loader;
use UDS\Component\Routing\RouteCollection;

/**
 * ClosureLoader loads routes from a PHP closure.
 *
 * The Closure must return a RouteCollection instance.
 *

 */
class ClosureLoader extends \UDS\Component\Config\Loader\Loader
{
    /**
     * Loads a Closure.
     */
    public function load(mixed $closure, string $type = null): \UDS\Component\Routing\RouteCollection
    {
        return $closure($this->env);
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        return $resource instanceof \Closure && (!$type || 'closure' === $type);
    }
}
