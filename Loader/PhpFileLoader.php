<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader;

use UDS\Component\Config\Loader\FileLoader;
use UDS\Component\Config\Resource\FileResource;
use UDS\Component\Routing\Loader\Configurator\RoutingConfigurator;
use UDS\Component\Routing\RouteCollection;

/**
 * PhpFileLoader loads routes from a PHP file.
 *
 * The file must return a RouteCollection instance.
 *



 */
class PhpFileLoader extends \UDS\Component\Config\Loader\FileLoader
{
    /**
     * Loads a PHP file.
     */
    public function load(mixed $file, string $type = null): \UDS\Component\Routing\RouteCollection
    {
        $path = $this->locator->locate($file);
        $this->setCurrentDir(\dirname((string) $path));

        // the closure forbids access to the private scope in the included file
        $loader = $this;
        $load = \Closure::bind(static fn($file) => include $file, null, \UDS\Component\Routing\Loader\ProtectedPhpFileLoader::class);

        $result = $load($path);

        if (\is_object($result) && \is_callable($result)) {
            $collection = $this->callConfigurator($result, $path, $file);
        } else {
            $collection = $result;
        }

        $collection->addResource(new \UDS\Component\Config\Resource\FileResource($path));

        return $collection;
    }

    public function supports(mixed $resource, string $type = null): bool
    {
        return \is_string($resource) && 'php' === pathinfo($resource, \PATHINFO_EXTENSION) && (!$type || 'php' === $type);
    }

    protected function callConfigurator(callable $result, string $path, string $file): \UDS\Component\Routing\RouteCollection
    {
        $collection = new \UDS\Component\Routing\RouteCollection();

        $result(new \UDS\Component\Routing\Loader\Configurator\RoutingConfigurator($collection, $this, $path, $file, $this->env));

        return $collection;
    }
}

/**
 * @internal
 */
final class ProtectedPhpFileLoader extends \UDS\Component\Routing\Loader\PhpFileLoader
{
}
