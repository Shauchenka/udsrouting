<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader\Configurator;

use UDS\Component\Routing\RouteCollection;

/**

 */
class ImportConfigurator
{
    use \UDS\Component\Routing\Loader\Configurator\Traits\HostTrait;
    use \UDS\Component\Routing\Loader\Configurator\Traits\PrefixTrait;
    use \UDS\Component\Routing\Loader\Configurator\Traits\RouteTrait;

    public function __construct(private readonly \UDS\Component\Routing\RouteCollection $parent, \UDS\Component\Routing\RouteCollection $route)
    {
        $this->route = $route;
    }

    public function __sleep(): array
    {
        throw new \BadMethodCallException('Cannot serialize '.self::class);
    }

    public function __wakeup()
    {
        throw new \BadMethodCallException('Cannot unserialize '.self::class);
    }

    public function __destruct()
    {
        $this->parent->addCollection($this->route);
    }

    /**
     * Sets the prefix to add to the path of all child routes.
     *
     * @param string|array $prefix the prefix, or the localized prefixes
     *
     * @return $this
     */
    final public function prefix(string|array $prefix, bool $trailingSlashOnRoot = true): static
    {
        $this->addPrefix($this->route, $prefix, $trailingSlashOnRoot);

        return $this;
    }

    /**
     * Sets the prefix to add to the name of all child routes.
     *
     * @return $this
     */
    final public function namePrefix(string $namePrefix): static
    {
        $this->route->addNamePrefix($namePrefix);

        return $this;
    }

    /**
     * Sets the host to use for all child routes.
     *
     * @param string|array $host the host, or the localized hosts
     *
     * @return $this
     */
    final public function host(string|array $host): static
    {
        $this->addHost($this->route, $host);

        return $this;
    }
}
