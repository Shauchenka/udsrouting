<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader\Configurator;

use UDS\Component\Routing\Loader\PhpFileLoader;
use UDS\Component\Routing\RouteCollection;

/**

 */
class RoutingConfigurator
{
    use \UDS\Component\Routing\Loader\Configurator\Traits\AddTrait;

    public function __construct(\UDS\Component\Routing\RouteCollection $collection, private readonly \UDS\Component\Routing\Loader\PhpFileLoader $loader, private string $path, private string $file, private readonly ?string $env = null)
    {
        $this->collection = $collection;
    }

    /**
     * @param string|string[]|null $exclude Glob patterns to exclude from the import
     */
    final public function import(string|array $resource, string $type = null, bool $ignoreErrors = false, string|array $exclude = null): \UDS\Component\Routing\Loader\Configurator\ImportConfigurator
    {
        $this->loader->setCurrentDir(\dirname($this->path));

        $imported = $this->loader->import($resource, $type, $ignoreErrors, $this->file, $exclude) ?: [];
        if (!\is_array($imported)) {
            return new \UDS\Component\Routing\Loader\Configurator\ImportConfigurator($this->collection, $imported);
        }

        $mergedCollection = new \UDS\Component\Routing\RouteCollection();
        foreach ($imported as $subCollection) {
            $mergedCollection->addCollection($subCollection);
        }

        return new \UDS\Component\Routing\Loader\Configurator\ImportConfigurator($this->collection, $mergedCollection);
    }

    final public function collection(string $name = ''): \UDS\Component\Routing\Loader\Configurator\CollectionConfigurator
    {
        return new \UDS\Component\Routing\Loader\Configurator\CollectionConfigurator($this->collection, $name);
    }

    /**
     * Get the current environment to be able to write conditional configuration.
     */
    final public function env(): ?string
    {
        return $this->env;
    }

    final public function withPath(string $path): static
    {
        $clone = clone $this;
        $clone->path = $clone->file = $path;

        return $clone;
    }
}
