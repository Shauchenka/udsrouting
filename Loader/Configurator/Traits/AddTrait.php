<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Loader\Configurator\Traits;

use UDS\Component\Routing\Loader\Configurator\AliasConfigurator;
use UDS\Component\Routing\Loader\Configurator\CollectionConfigurator;
use UDS\Component\Routing\Loader\Configurator\RouteConfigurator;
use UDS\Component\Routing\RouteCollection;

/**

 */
trait AddTrait
{
    use \UDS\Component\Routing\Loader\Configurator\Traits\LocalizedRouteTrait;

    /**
     * @var RouteCollection
     */
    protected $collection;
    protected $name = '';
    protected $prefixes;

    /**
     * Adds a route.
     *
     * @param string|array $path the path, or the localized paths of the route
     */
    public function add(string $name, string|array $path): \UDS\Component\Routing\Loader\Configurator\RouteConfigurator
    {
        $parentConfigurator = $this instanceof \UDS\Component\Routing\Loader\Configurator\CollectionConfigurator ? $this : ($this instanceof \UDS\Component\Routing\Loader\Configurator\RouteConfigurator ? $this->parentConfigurator : null);
        $route = $this->createLocalizedRoute($this->collection, $name, $path, $this->name, $this->prefixes);

        return new \UDS\Component\Routing\Loader\Configurator\RouteConfigurator($this->collection, $route, $this->name, $parentConfigurator, $this->prefixes);
    }

    public function alias(string $name, string $alias): \UDS\Component\Routing\Loader\Configurator\AliasConfigurator
    {
        return new \UDS\Component\Routing\Loader\Configurator\AliasConfigurator($this->collection->addAlias($name, $alias));
    }

    /**
     * Adds a route.
     *
     * @param string|array $path the path, or the localized paths of the route
     */
    public function __invoke(string $name, string|array $path): \UDS\Component\Routing\Loader\Configurator\RouteConfigurator
    {
        return $this->add($name, $path);
    }
}
