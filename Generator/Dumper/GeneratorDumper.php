<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Generator\Dumper;

use UDS\Component\Routing\RouteCollection;

/**
 * GeneratorDumper is the base class for all built-in generator dumpers.
 *

 */
abstract class GeneratorDumper implements \UDS\Component\Routing\Generator\Dumper\GeneratorDumperInterface
{
    public function __construct(private readonly \UDS\Component\Routing\RouteCollection $routes)
    {
    }

    public function getRoutes(): \UDS\Component\Routing\RouteCollection
    {
        return $this->routes;
    }
}
