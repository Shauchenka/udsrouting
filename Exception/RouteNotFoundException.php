<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Exception;

/**
 * Exception thrown when a route does not exist.
 *

 */
class RouteNotFoundException extends \InvalidArgumentException implements \UDS\Component\Routing\Exception\ExceptionInterface
{
}
