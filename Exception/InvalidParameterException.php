<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Exception;

/**
 * Exception thrown when a parameter is not valid.
 *

 */
class InvalidParameterException extends \InvalidArgumentException implements \UDS\Component\Routing\Exception\ExceptionInterface
{
}
