<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Exception;

/**
 * Exception thrown when no routes are configured.
 *

 */
class NoConfigurationException extends \UDS\Component\Routing\Exception\ResourceNotFoundException
{
}
