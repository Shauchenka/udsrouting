<?php

/*
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\DependencyInjection;

use UDS\Component\DependencyInjection\Compiler\CompilerPassInterface;
use UDS\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use UDS\Component\DependencyInjection\ContainerBuilder;
use UDS\Component\DependencyInjection\Reference;

/**
 * Adds tagged routing.loader services to routing.resolver service.
 *

 */
class RoutingResolverPass implements \UDS\Component\DependencyInjection\Compiler\CompilerPassInterface
{
    use \UDS\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;

    public function process(\UDS\Component\DependencyInjection\ContainerBuilder $container)
    {
        if (false === $container->hasDefinition('routing.resolver')) {
            return;
        }

        $definition = $container->getDefinition('routing.resolver');

        foreach ($this->findAndSortTaggedServices('routing.loader', $container) as $id) {
            $definition->addMethodCall('addLoader', [new \UDS\Component\DependencyInjection\Reference($id)]);
        }
    }
}
