<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Matcher;

use UDS\Component\Routing\Matcher\Dumper\CompiledUrlMatcherTrait;
use UDS\Component\Routing\RequestContext;

/**
 * Matches URLs based on rules dumped by CompiledUrlMatcherDumper.
 *

 */
class CompiledUrlMatcher extends \UDS\Component\Routing\Matcher\UrlMatcher
{
    use \UDS\Component\Routing\Matcher\Dumper\CompiledUrlMatcherTrait;

    public function __construct(array $compiledRoutes, \UDS\Component\Routing\RequestContext $context)
    {
        $this->context = $context;
        [$this->matchHost, $this->staticRoutes, $this->regexpList, $this->dynamicRoutes, $this->checkCondition] = $compiledRoutes;
    }
}
