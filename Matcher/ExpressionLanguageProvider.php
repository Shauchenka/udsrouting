<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Matcher;

use UDS\Component\ExpressionLanguage\ExpressionFunction;
use UDS\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use UDS\Contracts\Service\ServiceProviderInterface;

/**
 * Exposes functions defined in the request context to route conditions.
 *

 */
class ExpressionLanguageProvider implements \UDS\Component\ExpressionLanguage\ExpressionFunctionProviderInterface
{
    public function __construct(private readonly \UDS\Contracts\Service\ServiceProviderInterface $functions)
    {
    }

    public function getFunctions(): array
    {
        $functions = [];

        foreach ($this->functions->getProvidedServices() as $function => $type) {
            $functions[] = new \UDS\Component\ExpressionLanguage\ExpressionFunction(
                $function,
                static fn(...$args) => sprintf('($context->getParameter(\'_functions\')->get(%s)(%s))', var_export($function, true), implode(', ', $args)),
                fn($values, ...$args) => $values['context']->getParameter('_functions')->get($function)(...$args)
            );
        }

        return $functions;
    }

    public function get(string $function): callable
    {
        return $this->functions->get($function);
    }
}
