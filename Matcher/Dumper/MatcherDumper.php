<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Matcher\Dumper;

use UDS\Component\Routing\RouteCollection;

/**
 * MatcherDumper is the abstract class for all built-in matcher dumpers.
 *

 */
abstract class MatcherDumper implements \UDS\Component\Routing\Matcher\Dumper\MatcherDumperInterface
{
    public function __construct(private readonly \UDS\Component\Routing\RouteCollection $routes)
    {
    }

    public function getRoutes(): \UDS\Component\Routing\RouteCollection
    {
        return $this->routes;
    }
}
