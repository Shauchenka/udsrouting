<?php

/*
 
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace UDS\Component\Routing\Matcher;

use UDS\Component\Routing\Exception\ExceptionInterface;
use UDS\Component\Routing\Exception\ResourceNotFoundException;

/**

 */
abstract class RedirectableUrlMatcher extends \UDS\Component\Routing\Matcher\UrlMatcher implements \UDS\Component\Routing\Matcher\RedirectableUrlMatcherInterface
{
    public function match(string $pathinfo): array
    {
        try {
            return parent::match($pathinfo);
        } catch (\UDS\Component\Routing\Exception\ResourceNotFoundException $e) {
            if (!\in_array($this->context->getMethod(), ['HEAD', 'GET'], true)) {
                throw $e;
            }

            if ($this->allowSchemes) {
                redirect_scheme:
                $scheme = $this->context->getScheme();
                $this->context->setScheme(current($this->allowSchemes));
                try {
                    $ret = parent::match($pathinfo);

                    return $this->redirect($pathinfo, $ret['_route'] ?? null, $this->context->getScheme()) + $ret;
                } catch (\UDS\Component\Routing\Exception\ExceptionInterface) {
                    throw $e;
                } finally {
                    $this->context->setScheme($scheme);
                }
            } elseif ('/' === $trimmedPathinfo = rtrim($pathinfo, '/') ?: '/') {
                throw $e;
            } else {
                try {
                    $pathinfo = $trimmedPathinfo === $pathinfo ? $pathinfo.'/' : $trimmedPathinfo;
                    $ret = parent::match($pathinfo);

                    return $this->redirect($pathinfo, $ret['_route'] ?? null) + $ret;
                } catch (\UDS\Component\Routing\Exception\ExceptionInterface) {
                    if ($this->allowSchemes) {
                        goto redirect_scheme;
                    }
                    throw $e;
                }
            }
        }
    }
}
